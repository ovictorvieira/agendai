@extends('layouts.dashboard')

@section('content')
<div class="col-md-12 col-sm-12 col-lg-12 d-flex align-items-stretch grid-margin">
    <div class="row flex-grow">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="h2">Atualização de Cadastro</h4>
                    <p class="card-description card-title mb-2">Atualização de dados para se tornar uma automo</p>
                    <br>
                    <form class="forms-sample" action="{{route('atualizarParaAutonomo')}}" method="post">

                        <div id="loader" class="loader"></div>
                        <div id="tour-bg"></div>

                        {{ csrf_field() }}
                        <input type="hidden" name="id" value="{{--$users->id--}}">

                        <div class="row">

                            <div class="col-sm-12 col-md-5 col-lg-8">
                                <div class="card">
                                    <div class="card-body text-center">
                                        <img src="{{ asset('images/img/users/user.svg') }}" class="img-lg rounded-circle mb-2"/>
                                        <p class="text-muted">Cliente / Autonomo</p>
                                    </div>s
                                        <div class="form-group">
                                            <label for="value">CEP</label>
                                            <input type="text" class="form-control cep" maxlength="8" name="cep" id="cep">
                                        </div>
                                        <div class="form-group">
                                            <label for="value">Endereço</label>
                                            <input type="text" class="form-control" name="address" id="address">
                                        </div>
                                        <div class="form-group">
                                            <label for="value">Número</label>
                                            <input type="text" class="form-control" name="number" id="number">
                                        </div>
                                        <div class="form-group">
                                            <label for="value">Cidade</label>
                                            <input type="text" class="form-control" name="city" id="city">
                                        </div>
                                        <div class="form-group">
                                            <label for="value">Complemento</label>
                                            <input type="text" class="form-control" name="complement">
                                        </div>
                                        <p class="card-description text-left">Descrição</p>

                                        <textarea class="form-control" name="description" rows="5"></textarea>

                                        <div class="border-top pt-3">
                                            <div class="row">
                                                <div class="col-5 text-left">
                                                    <h6>Início do expediente</h6>
                                                    <input type="text" class="form-control time" name="start_time" required >
                                                </div>
                                                <div class="col-5 offset-2 text-left">
                                                    <h6>Fim do expediente</h6>
                                                    <input type="text" class="form-control time" name="end_time" required>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="pt-4">
                                            <div class="row">
                                                <div class="col-12">
                                                    <button type="submit" class="btn btn-info btn-fw btn-block">Tornar-me um Autonomo</button>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection