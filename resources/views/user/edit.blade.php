@extends('layouts.dashboard')

@section('content')
<div class="col-md-12 col-sm-12 col-lg-12 d-flex align-items-stretch grid-margin">
    <div class="row flex-grow">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="h2">Cadatrar Serviço</h4>
                    <hr>
                    <br>
                    <form class="forms-sample" action="{{route('atualizarPerfil')}}" method="post">

                        <div id="loader" class="loader"></div>
                        <div id="tour-bg"></div>

                        {{ csrf_field() }}
                        <input type="hidden" name="id" value="{{$users->id}}">

                        <div class="row">

                            <div class="col-sm-12 col-md-5 col-lg-5">
                                <div class="form-group">
                                    <label for="time">Nome</label>
                                    <input type="text" class="form-control" name="first_name" value="{{$users->first_name}}" required>
                                </div>
                                <div class="form-group">
                                    <label for="value">Sobrenome</label>
                                    <input type="text" class="form-control" name="last_name" value="{{$users->last_name}}" required>
                                </div>
                                <div class="form-group">
                                    <label for="value">E-mail</label>
                                    <input type="email" class="form-control"  name="email" value="{{$users->email}}" required>
                                </div>
                                <div class="form-group">
                                    <label for="value">Telefone</label>
                                    <input type="text" class="form-control phone_with_ddd"  name="telephone" value="{{$users->telephone}}" required>
                                </div>

                                @if($users->role_id == 2)

                                    <div class="form-group">
                                        <label for="value">CEP</label>
                                        <input type="text" class="form-control cep" maxlength="8" name="cep" id="cep">
                                    </div>
                                    <div class="form-group">
                                        <label for="value">Endereço</label>
                                        <input type="text" class="form-control" name="address" id="address" value="{{$users->address}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="value">Número</label>
                                        <input type="text" class="form-control" name="number" id="number" value="{{$users->number}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="value">Cidade</label>
                                        <input type="text" class="form-control" name="city" id="city" value="{{$users->city}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="value">Complemento</label>
                                        <input type="text" class="form-control" name="complement" value="{{$users->complement}}">
                                    </div>
                                @endif

                                <div class="row mt-4">
                                    <div class="col-sm-4 col-md-4 col-lg-4">
                                        <button type="submit" class="btn btn-success mr-2">Atualizar</button>
                                    </div>
                                </div>

                            </div>

                            <div class="col-sm-12 col-md-7 col-lg-7">
                                <div class="card text-center">
                                    <div class="card-body">
                                        <img src="{{ asset('images/img/users/user.svg') }}" class="img-lg rounded-circle mb-2"/>
                                        <h4>{{$users->first_name}} {{$users->last_name}}</h4>
                                        <p class="text-muted">Cliente / Autonomo</p>

                                        @if($users->role_id == 2)
                                            <p class="card-description text-left">Descrição</p>
                                            <textarea class="form-control" name="description" rows="5">{{$users->description}}</textarea>
                                            <div class="border-top pt-3">
                                                <div class="row">
                                                    <div class="col-5 text-left">
                                                        <h6>Início do expediente</h6>
                                                        <input type="text" class="form-control time" name="start_time" value="{{$users->start_time}}">
                                                    </div>
                                                    <div class="col-5 offset-2 text-left">
                                                        <h6>Fim do expediente</h6>
                                                        <input type="text" class="form-control time" name="end_time" value="{{$users->end_time}}">
                                                    </div>
                                                </div>
                                            </div>
                                            @else

                                            <div class="pt-4">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <a href="{{route( 'virarAutonomo', ['id' => $user->id])}}">
                                                            <button type="button" class="btn btn-info btn-fw">Tornar-me um Autonomo</button>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>

                                        @endif
                                    </div>
                                </div>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection