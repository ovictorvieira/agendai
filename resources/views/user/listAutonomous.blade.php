@extends('layouts.dashboard')
@section('content')
<h4 class="h2">Autonomos que prestão esse serviço</h4>
<div class="col-md-12 col-sm-12 col-lg-12 d-flex align-items-stretch grid-margin">
    <div class="row flex-grow">
        @foreach($autonomous as $autonomou)
            <div class="col-sm-4 col-md-4 col-lg-4 grid-margin stretch-card pl-2">
                <div class="card text-center">
                    <div class="card-body">
                        <img src="{{ asset('images/img/users/user.svg') }}" class="img-lg rounded-circle mb-4"/>
                        <h4>{{$autonomou['first_name'] . ' ' .  $autonomou['last_name']}}</h4>
                        <h5 class="card-title mb-0">{{$autonomou['name']}}</h5>

                        <p class="py-4 mt-0 card-text border-bottom">
                            {{$autonomou->description}}
                        </p>

                        <div class="border-bottom pt-1">
                            <div class="row">
                                <div class="col-3">
                                    <h6>Valor</h6>
                                    <p>{{$autonomou['value']}}</p>
                                </div>
                                <div class="col-4">
                                    <h6>Duração</h6>
                                    <p>
                                        {{$autonomou['time']}}
                                        <span class="sub-text-table">minutos</span>
                                    </p>
                                </div>
                                <div class="col-5">
                                    <h6>Telefone</h6>
                                    <p>{{$autonomou['telephone']}}</p>
                                </div>
                            </div>
                        </div>
                        <a href="{{route('horariosAutonomo', [
                                'idAutonomo' => $autonomou['userId'],
                                'date' => $date,
                                'serviceId' => $autonomou['serviceId']])}}">
                            <button class="btn btn-info btn-sm mt-4">Agendar</button>
                        </a>
                    </div>
                </div>
            </div>

        @endforeach
    </div>
</div>
@endsection