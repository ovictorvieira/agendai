@extends('layouts.dashboard')

@section('content')
<div class="col-md-12 col-sm-12 col-lg-12 grid-margin stretch-card">
    <div class="card">
        <div class="card-body">
            <h4 class="h2">Serviços Cadastrados</h4>
            <p class="card-description">
                Exibe todos os cadastros detalhados seus serviços cadastrados até o momento.
            </p>
            <hr>
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th class="font-weight-bold">Tipo de Serviço &nbsp;&nbsp;<i class="fas fa-chart-bar"></i></th>
                        <th class="font-weight-bold">Valor &nbsp;&nbsp;<i class="far fa-money-bill-alt"></i></th>
                        <th class="font-weight-bold">Duração do Serviço &nbsp;&nbsp;<i class="far fa-clock"></i></th>
                        <th class="font-weight-bold">Edições &nbsp;&nbsp;<i class="fas fa-edit"></i></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($services as $service)
                        <tr>
                            <td>{{$service->name}}</td>
                            <td><span class="sub-text-table">R$&nbsp;</span><span class="text-success ">{{$service->value}}</span></td>
                            <td>{{$service->time}} <span class="sub-text-table">minutos</span></td>
                            <td>
                                <a href="{{route('atualizarServico', ['id'=> $service->id])}}">
                                    <button class="btn btn-info btn-sm p-1">Editar <i class="fas fa-edit mx-0"></i></button>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="row mt-4">
                <div class="col-sm-4 col-md-4 col-lg-4">
                    <a href="{{route('cadastrarServico')}}">
                        <button type="submit" class="btn btn-success mr-2">Cadastrar Novo Serviço</button>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection