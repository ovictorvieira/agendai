@extends('layouts.dashboard')
@section('content')
<h4 class="h2">Agendar</h4>
<div class="col-md-12 col-sm-12 col-lg-12 d-flex align-items-stretch grid-margin">
    <form action="{{route('criarAgendamento')}}" method="POST" class="form-control">
        {{ csrf_field() }}

        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-5 col-md-5 col-lg-5 grid-margin stretch-card pl-2">
                    <div class="card text-center">
                        <div class="card-body">
                            <img src="{{ asset('images/img/users/user.svg') }}" class="img-lg rounded-circle mb-4"/>
                            <h4>{{$service->first_name . ' ' . $service->last_name}}</h4>
                            <h5 class="card-title mb-0">{{$service->name}}</h5>

                            <p class="py-4 mt-0 card-text border-bottom">
                                {{$service->description}}
                            </p>

                            <div class="border-bottom pt-1">
                                <div class="row">
                                    <div class="col-3">
                                        <h6>Valor</h6>
                                        <p>{{$service->value}}</p>
                                    </div>
                                    <div class="col-4">
                                        <h6>Duração</h6>
                                        <p>
                                            {{$service->time}}
                                            <span class="sub-text-table">minutos</span>
                                        </p>
                                    </div>
                                    <div class="col-5">
                                        <h6>Data</h6>
                                        <p>{{$date}}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-2 col-md-2 col-lg-2"></div>
                <div class="col-sm-4 col-md-4 col-lg-4 align-items-center grid-margin stretch-card">
                    <div class="w-100">
                        <div class="form-group">
                            <label for="service_id">Horários Disponíveis</label>
                            <select class="form-control" name="time" id="time">
                                @foreach($freeTimes as $freeTime)
                                    <option>{{$freeTime}}</option>
                                @endforeach
                            </select>
                        </div>
                        <input type="hidden" name="client_id" value="{{$user->id}}">
                        <input type="hidden" name="autonomous_id" value="{{$service->autonomousId}}">
                        <input type="hidden" name="service_id" value="{{$service->serviceId}}">
                        <input type="hidden" name="date" value="{{$date}}">

                        <div class="row mt-4">
                            <div class="col-sm-4 col-md-4 col-lg-4">
                                <button type="submit" class="btn btn-success mr-2">Agendar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection