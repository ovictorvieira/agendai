<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" href="{{ asset('images/img/icons/calendar.png') }}"/>

    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Catamaran:100,200,300,400,500,600,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Muli" rel="stylesheet">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Agendaí - Plataforma Para Agendamentos de Serviço</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/vendor.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('css/template.css') }}" rel="stylesheet">
    <link href="{{ asset('css/device-mockups.css') }}" rel="stylesheet">
    <link href="{{ asset('css/main-template.css') }}" rel="stylesheet">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />

</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
            <div class="container">
                <a class="navbar-brand js-scroll-trigger" href="http://agendai.local/">Agendaí</a>
                <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    Agendaí
                    <i class="fa fa-bars"></i>
                </button>

                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link js-scroll-trigger" href="{{ route('login') }}">Login</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link js-scroll-trigger" href="{{ route('register') }}">Registrar</a>
                            </li>
                        @else
                            <ul>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true" v-pre>
                                        {{ Auth::user()->name }} <span class="caret"></span>
                                    </a>

                                    <ul class="dropdown-menu">
                                        <li>
                                            <a href="{{ route('logout') }}"
                                               onclick="event.preventDefault();
                                                             document.getElementById('logout-form').submit();">
                                                Logout
                                            </a>
                                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                {{ csrf_field() }}
                                            </form>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        @endguest

                    </ul>
                </div>
            </div>
        </nav>

        @yield('content')

        <footer id="contact">
            <div class="container footer-main">
                <div class="row">
                    <div class="col-sm-3 text-left footer-info">
                        <div class="footer-logo"></div>
                        <p>
                            A agendaí é uma ferramenta focada em agilizar e organizar os agendamentos de sua
                            empresa ou de seu negócio.
                        </p>
                        <ul class="footer-social-list">
                            <li>
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                            </li>
                            <li>
                                <a href="#"><i class="fab fa-linkedin-in"></i></a>
                            </li>
                            <li>
                                <a href="#"><i class="fab fa-twitter"></i></a>
                            </li>
                            <li>
                                <a href="#"><i class="fab fa-instagram"></i></a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-sm-3 text-left footer-info footer-contact">
                        <h4>Contato</h4>
                        <p>
                            Marília, SP
                            <br> Brasil
                            <br>
                        </p>
                        <p>
                            <a class="text-body" href="mailto:overplay@gmail.com">agendai@gmail.com</a>
                            <br> Telefone: (14) 0000-0000
                            <br> Whatsapp: (14) 0 0000-0000
                        </p>
                    </div>
                    <div class="col-sm-2 text-left footer-info footer-site-links">
                        <h4>Navegação</h4>
                        <ul class="list-links">
                            <li><a href="#download" class="js-scroll-trigger">Download</a></li>
                            <li><a href="#features" class="js-scroll-trigger">Funcionalidades</a></li>
                            <li><a href="#cta-section" class="js-scroll-trigger">Agendar Agora</a></li>
                            <li><a href="#team" class="js-scroll-trigger">Nosso Time</a></li>
                            <li><a href="#contact" class="js-scroll-trigger">Fale Conosco</a></li>
                        </ul>

                    </div>
                    <div class="col-sm-4 text-left footer-info footer-subscribe">

                        <h4>Entre em contato</h4>

                        <p>Estamos a disposição para esclarecer suas dúvidas.</p>

                        <div id="messege-sucess">
                            <div class="alert alert-success alert-custom" role="alert">
                                Mensagem enviada com sucesso.
                            </div>
                        </div>
                        <div id="messege-fail">
                            <div class="alert alert-warning" role="alert">
                                Insira um email válido.
                            </div>
                        </div>

                        <div class="subscribe-form">

                            <form id="mc-form" class="group" novalidate="true">

                                <input type="email" value="" name="email" class="email" id="mc-email" placeholder="E-mail" required="">

                                <input type="submit" name="subscribe" class="subscribe" value="Enviar" onclick="sendMessege()" autocomplete="off">

                                <label for="mc-email" class="subscribe-message"></label>

                            </form>

                        </div>

                    </div>
                </div>
            </div>

            <div class="container footer-bottom">
                <span>© Copyright Agendaí <?php echo date('Y') ?>.</span>
            </div>
        </footer>
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/vendor.js') }}"></script>
    <script src="{{ asset('js/bootstrap.js') }}"></script>
    <script src="{{ asset('js/main.js') }}"></script>
    <script src="{{ asset('js/mask.js') }}"></script>

    </body>
</html>

