<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" href="{{ asset('images/img/icons/calendar.png') }}"/>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Agendaí - Plataforma Para Agendamentos de Serviço</title>

    <!-- Styles -->
    <link href="{{ asset('css/dashboard.css') }}" rel="stylesheet">
    <link href="{{ asset('css/dashboard-template.css') }}" rel="stylesheet">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />

</head>
    <body>
        <div class="container-scroller">
            <nav class="navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
                <div id="dashboard-navbar" class="text-center navbar-brand-wrapper">
                    <a href="{{route( 'home' )}}" class="navbar-brand js-scroll-trigger">Agendaí</a>
                </div>
                <div class="navbar-menu-wrapper d-flex align-items-center">
                    <p class="page-name d-none d-lg-block">Bem vindo, {{$user->first_name . ' ' . $user->last_name}}</p>
                    <ul class="navbar-nav ml-lg-auto">
                        <li class="nav-item d-none d-sm-block profile-img">
                            <a class="nav-link profile-image" href="#">
                                <img src="{{ asset('images/img/users/user.svg') }}" alt="profile-img">
                                <span class="online-status online bg-success"></span>
                            </a>
                        </li>
                    </ul>
                    <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center ml-auto" type="button" data-toggle="offcanvas">
                        <span class="icon-menu icons"></span>
                    </button>
                </div>
            </nav>

            <div class="container-fluid page-body-wrapper">
                <div class="row row-offcanvas row-offcanvas-right">
                    <nav class="sidebar sidebar-offcanvas" id="sidebar">
                        <ul class="nav">
                            <li class="nav-item nav-category">
                                <span class="nav-link">Menu de Opções</span>
                            </li>
                            <li class="nav-item active">
                                <a class="nav-link" href="{{route( 'home' )}}">
                                    <span class="menu-title text-uppercase">Início</span>
                                    <i class="fas fa-home"></i>
                                </a>
                            </li>
                            @if($user->role_id == 1)
                                <li class="nav-item">
                                    <a class="nav-link" href="{{route( 'virarAutonomo', ['id' => $user->id])}}">
                                        <span class="menu-title text-uppercase">Tornar-me um Autonomo</span>
                                        <i class="fas fa-chart-bar"></i>
                                    </a>
                                </li>
                            @elseif ($user->role_id == 2)
                                <li class="nav-item">
                                    <a class="nav-link" href="{{route( 'listarServicos' )}}">
                                        <span class="menu-title text-uppercase">Serviços Cadastrados</span>
                                        <i class="fas fa-address-book"></i>
                                    </a>
                                </li>
                            @endif
                            <li class="nav-item">
                                <a class="nav-link" href="{{route( 'listaServicos' )}}">
                                    <span class="menu-title text-uppercase">Agendar Agora</span>
                                    <i class="far fa-calendar-check"></i>
                                </a>
                            </li>
                            <li class="nav-item d-none">
                                <a class="nav-link" href="">
                                    <span class="menu-title text-uppercase">Meus Agendamentos</span>
                                    <i class="fas fa-calendar-alt"></i>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{route( 'editarPerfil' )}}">
                                    <span class="menu-title text-uppercase">Meu Perfil</span>
                                    <i class="fas fa-edit"></i>
                                </a>
                            </li>
                            <li class="nav-item">
                                <!-- Authentication Links -->
                                @guest

                                @else
                                    <a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                        <span class="menu-title text-uppercase">Sair</span>
                                        <i class="fa fa-power-off"></i>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                        @endguest

                                    </a>
                            </li>
                        </ul>
                    </nav>
                    <div class="content-wrapper">
                        <div class="row">
                            @if(session()->has('msg'))
                                <div class="col-md-12 col-md-offset-2">
                                    <div class="alert alert-success" role="alert">
                                        <strong>Sucesso!</strong> {{session('msg')}}.
                                    </div>
                                </div>
                            @elseif(session()->has('msgError'))
                                <div class="col-md-12 col-md-offset-2">
                                    <div class="alert alert-success" role="alert">
                                        <strong>Erro!</strong> {{session('msg')}}.
                                    </div>
                                </div>
                            @endif
                            @yield('content')
                        </div>
                    </div>
                    <footer class="footer">
                        <div class="container-fluid clearfix text-center">
                            <span class="text-muted d-block d-sm-inline-block">© Copyright Agendaí <?php echo date('Y') ?>. All rights reserved.</span>
                        </div>
                    </footer>
                </div>
            </div>
        </div>
        <!-- Scripts -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.1/moment.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.slim.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/highlight.min.js"></script>

        <script src="{{ asset('js/dashboard.js') }}"></script>
        <script src="{{ asset('js/mask.js') }}"></script>
        <script src="{{ asset('js/viacep.js') }}"></script>
    </body>
</html>

