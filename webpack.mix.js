let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.styles([
    'public/css/custom/css/css-template/util-template.css',
    'public/css/custom/device-mockups/device-mockups.css',
    'public/css/custom/css/css-template/app-template.css',
    'public/css/custom/fonts/Linearicons-Free-v1.0.0/icon-font.min.css',
    'public/css/custom/css/animate/animate.css',
    'public/css/custom/css/css-hamburgers/hamburgers.css',
    'public/css/custom/css/animsition/css/animsition.css',
    'public/css/custom/css/login/main-login.css',
    'public/css/custom/css/login/util-login.css',
    'public/css/custom/css/daterangepicker/daterangepicker.css'
], 'public/css/vendor.css');

mix.styles('public/css/custom/css/bootstrap/css/bootstrap.css','public/css/bootstrap.css');
mix.styles('public/css/custom/css/css-template/main-template.css','public/css/main-template.css');
mix.styles('public/css/custom/device-mockups/device-mockups.css','public/css/device-mockups.css');
mix.scripts('public/js/custom/bootstrap/js/bootstrap.js','public/js/bootstrap.js');
mix.scripts('public/js/custom/theme/main.min.js','public/js/main.js');

mix.scripts([
    'public/js/custom/jquery/jquery-3.2.1.min.js',
    'public/js/custom/jquery-easing/jquery.easing.js',
    'public/js/custom/perfect-scrollbar/perfect-scrollbar.min',
    'public/js/custom/animsition.js',
    'public/js/custom/login/main-login.js',
    'public/js/custom/countdowntime/countdowntime.js',
    'public/js/custom/daterangepicker/daterangepicker.js',
    'public/js/custom/daterangepicker/moment.js',
    'public/js/custom/select2/select2.js'
], 'public/js/vendor.js');

mix.scripts([
    'public/js/custom/dashboard/dashboard.docs.js',
    'public/js/custom/dashboard/misc.js',
    'public/js/custom/dashboard/off-canvas.js',
    'public/js/custom/dashboard/dashboard.min.js',
    'public/js/custom/jquery/jquery-3.2.1.min.js'
], 'public/js/dashboard.js');

mix.scripts([
    'public/js/custom/mask/jquery.mask.min.js',
    'public/js/custom/mask/mask.js',
], 'public/js/mask.js');

mix.scripts([
    'public/js/custom/viacep/viacep.js'
], 'public/js/viacep.js');

mix.styles(['public/css/scss/_bootstrap-overrides.scss',
            'public/css/scss/_contact.scss',
            'public/css/scss/_cta.scss',
            'public/css/scss/_download.scss',
            'public/css/scss/_features.scss',
            'public/css/scss/_footer.scss',
            'public/css/scss/_global.scss',
            'public/css/scss/_masthead.scss',
            'public/css/scss/_mixins.scss',
            'public/css/scss/_navbar.scss',
            'public/css/scss/_variables.scss'
    ],'public/css/template.css');

mix.styles([
    'public/css/scss/_misc_dash.scss',
    'public/css/scss/_variables-dash.scss'
],'public/css/dashboard-template.css');

mix.styles([
    'public/css/custom/css/dashboard/dashboard-css.css'
],'public/css/dashboard.css');

 mix.js('resources/assets/js/app.js', 'public/js')
    .sass('resources/assets/sass/app.scss', 'public/css');
