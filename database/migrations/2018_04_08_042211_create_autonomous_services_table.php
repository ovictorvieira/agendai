<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAutonomousServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('autonomous_services', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('autonomous_id');
            $table->unsignedInteger('service_id');
            $table->decimal('value', 8,2);
            $table->string('time');
            $table->timestamps();

            $table->foreign('autonomous_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('service_id')->references('id')->on('services')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('autonomous_services');
    }
}
