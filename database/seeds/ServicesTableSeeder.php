<?php

use Illuminate\Database\Seeder;
use App\Services;

class ServicesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cortarCabelo = [
            'name'=> 'Cortar Cabelo'
        ];
        Services::create($cortarCabelo);

        $cortarBarba = [
            'name'=> 'Cortar Barba'
        ];
        Services::create($cortarBarba);

        $fazerUnhas = [
            'name'=> 'Fazer unhas da mão'
        ];
        Services::create($fazerUnhas);
    }
}