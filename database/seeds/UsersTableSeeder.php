<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cliente = [
            'first_name'=> 'Cliente',
            'last_name' => 'Cliente',
            'email' => 'cliente@cliente.com',
            'password' => bcrypt('123'),
            'telephone' => '99999999999',
            'role_id' => 1
        ];

        User::create($cliente);

        $autonomo = [
            'first_name'=> 'Autonomo',
            'last_name' => 'Autonomo',
            'email' => 'autonomo@autonomo.com',
            'password' => bcrypt('123'),
            'telephone' => '99999999999',
            'role_id' => 2
        ];
        User::create($autonomo);
    }
}
