<?php
/**
 * Created by PhpStorm.
 * User: leonardo
 * Date: 04/06/18
 * Time: 18:57
 */

namespace App\Repositories;

use App\Services;
use App\AutonomousServices;

class ServiceRepository
{
    protected $services;

    protected $autonomousServices;

    public function __construct(Services $services, AutonomousServices $autonomousServices)
    {
        $this->services             = $services;
        $this->autonomousServices   = $autonomousServices;
    }

    public function create($data)
    {
        return $this->autonomousServices->create($data);
    }

    public function getByIdToUpdate($id)
    {
        return $this->autonomousServices->findOrFail($id);
    }

    public function getServiceByIdAndUserId($id, $userId)
    {
        return $this->autonomousServices
            ->where('service_id', '=', $id)
            ->where('autonomous_id', '=', $userId)
            ->get()
            ->first();
    }

    public function getById($id)
    {
        return $this->services
            ->select('autonomous_services.value','autonomous_services.time', 'services.name', 'services.id', 'autonomous_services.id as autonomous_serviceId')
            ->join('autonomous_services', 'autonomous_services.service_id', 'services.id')
            ->where('autonomous_services.id', '=', $id)->get()->all();
    }

    public function getAllByUserId($userId)
    {
        return $this->services
            ->select('autonomous_services.value','autonomous_services.time', 'services.name', 'autonomous_services.id')
            ->join('autonomous_services', 'autonomous_services.service_id', 'services.id')
            ->where('autonomous_services.autonomous_id', '=', $userId)->get()->all();
    }

    public function getAll()
    {
        return $this->services->all();
    }

    public function update($data, $id)
    {
        $servicesAutonomous = $this->getByIdToUpdate($id);
        return $servicesAutonomous->fill($data)->save();
    }

    public function getByServiceId($serviceId, $autonomoId)
    {
        return $this->services
            ->select(
                'autonomous_services.value',
                'autonomous_services.time',
                'services.name',
                'users.first_name',
                'users.description',
                'users.last_name',
                'services.id as serviceId',
                'autonomous_services.autonomous_id as autonomousId'
            )
            ->join('autonomous_services', 'autonomous_services.service_id', 'services.id')
            ->join('users', 'users.id', 'autonomous_services.autonomous_id')
            ->where('autonomous_services.service_id', '=', $serviceId)
            ->where('autonomous_services.autonomous_id', '=', $autonomoId)
            ->get()->all();
    }
}