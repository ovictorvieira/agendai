<?php

namespace App\Repositories;

use App\Role;

class RoleRepository
{
    protected $roleModel;

    public function __construct(Role $roleModel)
    {
        $this->roleModel = $roleModel;
    }

    public function getAllRole()
    {
        return $this->roleModel->all();
    }

    public function getRoleToId($id)
    {
        return $this->roleModel->findOrFail($id);
    }

}