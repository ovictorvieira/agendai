<?php

namespace App\Repositories;

use App\User;
use Caffeinated\Repository\Repositories\EloquentRepository;

class UserRepository extends EloquentRepository
{
    protected $userModel;

    public function __construct(User $user)
    {
        $this->userModel = $user;
    }

    public function getUserAll()
    {
        return $this->userModel->all();
    }

    public function getUserById($id)
    {
        return $this->userModel->findOrFail($id);
    }

    public function createUser($data)
    {
        return $this->userModel->create($data);
    }

    public function updateUser($id, $data)
    {
        $userUpdate = $this->getUserById($id);
        return $userUpdate->fill($data)->save();
    }

    public function deleteUser($id)
    {
        return $this->destroy($id);
    }

    public function getUserByServiceId($serviceId)
    {
        return $this->userModel->select(
            'users.description',
            'users.id as userId',
            'services.id as serviceId',
            'users.first_name',
            'users.last_name',
            'users.telephone',
            'services.name',
            'autonomous_services.time',
            'autonomous_services.value')
            ->where('services.id', '=', $serviceId)
            ->join('autonomous_services', 'users.id', 'autonomous_services.autonomous_id')
            ->join('services', 'services.id', 'autonomous_services.service_id')->get();

    }
}