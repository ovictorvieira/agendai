<?php

namespace App\Services;

use App\Repositories\ServiceRepository;

class ServicesServices
{
    protected $serviceRepository;

    public function __construct(ServiceRepository $serviceRepository)
    {
        $this->serviceRepository = $serviceRepository;
    }

    public function getAllByUserId($userId)
    {
        return $this->serviceRepository->getAllByUserId($userId);
    }

    public function getAll()
    {
        return $this->serviceRepository->getAll();
    }

    public function store(array $data)
    {
        return $this->serviceRepository->create($data);
    }

    public function getById($id)
    {
        return $this->serviceRepository->getById($id);
    }

    public function update($data, $id)
    {
        return $this->serviceRepository->update($data, $id);
    }

    public function getByServiceId($serviceId, $autonomousId)
    {
        return $this->serviceRepository->getByServiceId($serviceId, $autonomousId);
    }
}