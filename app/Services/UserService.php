<?php

namespace App\Services;

use App\Repositories\ScheduleRepository;
use App\Repositories\ServiceRepository;
use App\Repositories\UserRepository;
use Carbon\Carbon;

class UserService
{
    protected $userRepository;
    protected $schedulesRepository;
    protected $serviceRepository;

    public function __construct( UserRepository $userRepository, ScheduleRepository $scheduleRepository, ServiceRepository $servicesServices)
    {
        $this->userRepository = $userRepository;
        $this->schedulesRepository = $scheduleRepository;
        $this->serviceRepository = $servicesServices;
    }

    public function create($data)
    {
        return $this->userRepository->createUser($data);
    }

    public function getUserById($id)
    {
        return $this->userRepository->getUserById($id);
    }

    public function update($data, $id)
    {
        return $this->userRepository->updateUser($id, $data);
    }

    public function getUserByServiceId($serviceId)
    {
        return $this->userRepository->getUserByServiceId($serviceId);
    }

    public function timeFree($autonomoId, $date, $serviceId)
    {
        $schedules = $this->schedulesRepository->getByAutonomousId($autonomoId, $date);
        $autonomo = $this->userRepository->getUserById($autonomoId);
        $serviceAutonomous = $this->serviceRepository->getServiceByIdAndUserId($serviceId, $autonomoId);

        $totServicePerHour = intval(60/$serviceAutonomous->time);

        $startTime = intval($autonomo->start_time);
        $endTime = intval($autonomo->end_time);

        $freeTime = [];
        // Preencher com todos os horários
        for ($hour=$startTime; $hour < $endTime;  $hour++) {
            $aux = 0;
            for ($i = 0; $i <=$totServicePerHour;  $i++) {
                if ($aux > 55) {
                    break;
                }
                if ($aux < 10) {
                    if ($hour < 10) {
                        $freeTime[] = '0' . $hour. ':0' . $aux . ':00';
                    } else {
                        $freeTime[] = $hour. ':0' . $aux . ':00';
                    }
                } else {
                    if ($hour < 10) {
                        $freeTime[] = '0' . $hour. ':' . $aux . ':00';
                    } else {
                        $freeTime[] = $hour. ':' . $aux . ':00';
                    }
                }
                $aux += $serviceAutonomous->time;
            }
        }

        // Remove os horários já agendados.
        foreach ($schedules as $schedule) {
            if (in_array($schedule->time, $freeTime)) {
                unset($freeTime[array_search($schedule->time, $freeTime)]);
            }
        }
        return $freeTime;
    }
}
