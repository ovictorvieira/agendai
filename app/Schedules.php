<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Schedules extends Model
{
    protected $fillable = [
        'client_id', 'autonomous_id', 'time', 'date', 'service_id'
    ];

    protected $dates = ['date'];
}
