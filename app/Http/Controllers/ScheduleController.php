<?php

namespace App\Http\Controllers;

use App\Services\ScheduleServices;
use Illuminate\Http\Request;

class ScheduleController extends Controller
{
    protected $scheduleService;

    public function __construct(ScheduleServices $scheduleServices)
    {
        $this->scheduleService = $scheduleServices;
    }

    public function create(Request $request)
    {
        $inputs = $request->all();
        if($this->scheduleService->create($inputs)) {
            return redirect('/home')->with('msg', 'Agendamento criado com sucesso');
        }
        return redirect('/home')->with('msgError', 'Erro ao criar agendamento');
    }

    public function getScheduleByUserId($userId)
    {
        return $this->scheduleService->getScheduleByUserId($userId);
    }
}
